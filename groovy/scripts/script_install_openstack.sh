#!/bin/bash

rm -r -f /opt/stack/devstack
echo "cloning devstack"

git clone https://opendev.org/openstack/devstack /opt/stack/devstack
cd /opt/stack/devstack
echo "[[local|localrc]]
ADMIN_PASSWORD=admin
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD" > local.conf
/opt/stack/devstack/tools/create-stack-user.sh

