#!/bin/bash

rm -f res_jenkins.txt
echo "ubuntu style"
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key
sudo apt-get update
sudo apt-cache search jenkins
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCEF32E745F2C3D5
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \/etc/apt/sources.list.d/jenkins.list'
sudo apt-get install jenkins* python*jenkins default-jre -y
path=`type -p java`
cp /etc/bash.bashrc .
echo "PATH=${path}:$PATH" >> bash.bashrc
sudo mv bash.bashrc /etc/bash.bashrc

echo "success" > res_jenkins.txt

