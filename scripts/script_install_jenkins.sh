#!/bin/bash

echo "ubuntu style"
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key
apt-get update
apt-cache search jenkins
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCEF32E745F2C3D5
sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \/etc/apt/sources.list.d/jenkins.list'
apt-get install jenkins* python*jenkins default-jre -y
path=`type -p java`
cp /etc/bash.bashrc .
echo "PATH=${path}:$PATH" >> bash.bashrc
mv bash.bashrc /etc/bash.bashrc

echo "success" > res_jenkins.txt

