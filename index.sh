#!/bin/bash

if [ ${#} -eq 0 ];then 
	echo "full devOps install"
	./scripts/script_sysupdate.sh;
	./scripts/script_install_openstack.sh
	./scripts/script_install_jenkins.sh
	./scripts/script_install_git.sh


elif [ "$1" == "install" ]
then
	if [ -z "$2" ];then echo "please specify the package you want to install";
	elif [ $2 = "openstack" ];then ./scripts/script_install_openstack.sh;
	elif [ $2 = "jenkins" ];then ./scripts/script_install_jenkins.sh;
	elif [ $2 = "git" ];then ./scripts/script_install_git.sh;
	else ./scripts/script_install_pkg.sh ${2}
	fi

elif [ "$1" == "user" ]
then
	if [ -z "$2" ];then echo "please specify the user name you want to create";
	else ./scripts/script_add_user.sh ${2}
	fi

elif [ "$1" == "group" ]
then
	if [  -z "$2" ];then echo "please specify the group name you want to create";
	else ./scripts/script_add_group.sh ${2}
	fi

elif [ "$1" == "restart" ]
then
	if [  -z "$2" ];then echo "please specify the service name you want to restart";
	else ./scripts/script_restart_service.sh ${2}
	fi
fi


